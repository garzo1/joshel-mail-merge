// Joshel,
// I created this script to merge data from a spreadsheet hosted on Google Apps into newly created files that you can print,
// stuff into an envelope and mail.  let me know if I can help explain anything.

function createDocFromSheet() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var docName = sheet.getRange("K2:K17").getValues();
  //NOTES: 
  // New documents might not have as many, or have more than 17 rows. You'll have to edit this part by hand to make changes.
  // the first row is the header row, so always offset by 1 for your starting cell.

  // for example, using the file '2015 06 22 Travis County Probates (TX).xls' 
  // the first row with last names is F2, and the last row is F17
  
  var lastName = sheet.getRange("F2:F17").getValues();  
  
  // same thing with propertyAddress, city and zip code.
  
  var propAddress = sheet.getRange("K2:K17").getValues();
  var city = sheet.getRange("L2:L17").getValues();
  var zipcode = sheet.getRange("N2:N17").getValues();
  var lastRow = sheet.getLastRow();
  

  for (var i = 0; i < lastRow; i++) {
    var doc = DocumentApp.create(docName[i]);
    var body = doc.getBody();
    
    body.appendParagraph("Dear " + lastName[i] + " Family,\n\n" + 
                         "Hi, my name is Joshel Brown.\n\n " + 
                         "I would like to purchase your property at " + propAddress[i] + ", " + city[i] +" Texas, " + zipcode[i] + ". " +
                         "Please let me know if you are interested in selling, or if you know anyone in the area who may be. \n\n" +
                         "You may reach me at 555-555-5555. I do look forward to hearing from you soon \n\n" +
                         "Best regards, \n\nJoshel Brown");                   
    

  }
}